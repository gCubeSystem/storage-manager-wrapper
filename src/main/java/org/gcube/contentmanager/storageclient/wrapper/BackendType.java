package org.gcube.contentmanager.storageclient.wrapper;

public enum BackendType {
	MongoDB, S3;
	
	@Override
	  public String toString() {
	    switch(this) {
	      case MongoDB: return "MongoDB";
	      case S3: return "S3";
	      default: throw new IllegalArgumentException();
	    }
	  }
	
}
