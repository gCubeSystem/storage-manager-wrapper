package org.gcube.contentmanager.storageclient.protocol.utils;

import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.scope.impl.ServiceMapScannerMediator;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * Utility class for scope identity
 *
 * @author Roberto Cirillo (ISTI-CNR)
 
 */
public class Utils {
	
	// Temporary CONSTANTS	
		private static final String GCUBE_RESOLVER_HOST = "data.gcube.org";
		private static final String D4SCIENCE_RESOLVER_HOST = "data.d4science.org";	
		private static final String GCUBE_INFRA = "gcube";
		private static final String D4SCIENCE_INFRA = "d4science.research-infrastructures.eu";
		public static final String INFRASTRUCTURE_ENV_VARIABLE_NAME="infrastructure";
		public static final String  URI_RESOLVER_RESOURCE_CATEGORY="Service";
		public static final String  URI_RESOLVER_RESOURCE_NAME="HTTP-URI-Resolver";
	
	private static final Logger logger = LoggerFactory.getLogger(Utils.class);



	
	public static String getInfraFromResolverHost(String resolverHost) {
		if(resolverHost.equals(GCUBE_RESOLVER_HOST)){
			return GCUBE_INFRA;
		}else if(resolverHost.equals(D4SCIENCE_RESOLVER_HOST)){
			return D4SCIENCE_INFRA;
		}else return resolverHost;
	}


	public static List<ServiceEndpoint> queryServiceEndpoint(String category, String name){
		SimpleQuery query = queryFor(ServiceEndpoint.class);
		query.addCondition("$resource/Profile/Category/text() eq '"+category+"' and $resource/Profile/Name eq '"+name+"' ");
		DiscoveryClient<ServiceEndpoint> client = clientFor(ServiceEndpoint.class);
		List<ServiceEndpoint> resources = client.submit(query);
		return resources;
	}
	
	public static String getResolverHost(ServiceEndpoint serviceEndpoint) {
		return serviceEndpoint.profile().runtime().hostedOn();
		
	}

}
