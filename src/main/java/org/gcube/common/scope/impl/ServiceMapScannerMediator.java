package org.gcube.common.scope.impl;

import java.util.Set;



/**
 * Utility class
 * @author Roberto Cirillo (ISTI-CNR)
 *
 */
public class ServiceMapScannerMediator {

	
	
	public static Set<String> getScopeKeySet(){
		return ServiceMapScanner.maps().keySet();
	}

}
