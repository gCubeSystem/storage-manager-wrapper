package org.gcube.contentmanager.storageclient.test;

import static org.junit.Assert.*;
import java.io.File;
import java.util.List;

import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.contentmanagement.blobstorage.resource.StorageObject;
import org.gcube.contentmanagement.blobstorage.service.IClient;
import org.gcube.contentmanagement.blobstorage.transport.backend.RemoteBackendException;
import org.gcube.contentmanager.storageclient.test.utils.Costants;
import org.gcube.contentmanager.storageclient.wrapper.AccessType;
import org.gcube.contentmanager.storageclient.wrapper.StorageClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ForceCloseTest {

	
	private String owner="rcirillo";
	private String localPath="src/test/resources/CostaRica.jpg";
	private String localPath2="src/test/resources/dog.jpg";
	private String localPathDownload="src/test/resources/download.png";
	private String remotePath="/test/test-resources/CostaRica1.jpg";
	private String absoluteLocalPath;
	private String absoluteLocalPath2;
	private String absoluteLocalPathDownload;
	private String newFilePath="src/test/resources";
	private String remoteDirPath= "test/test-forceclose";
	private IClient client;
	private String serviceClass="JUnitTest-ForceCloseTest";
	private String serviceName="StorageManager";
	private static final Logger logger = LoggerFactory.getLogger(ForceCloseTest.class);
	
	@Before
	public void getClient(){
		SecretManagerProvider.set(new ICSecret(Costants.DEFAULT_SCOPE_STRING));
		try {
			client=new StorageClient(serviceClass, serviceName, owner, AccessType.SHARED, Costants.DEFAULT_MEMORY_TYPE).getClient();
			assertNotNull(client);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setLocalResources();
	//upload costaRica
		String id=client.put(true).LFile(absoluteLocalPath).RFile(remotePath);
		System.out.println("UploadByPath id: "+id);
		assertNotNull(id);
	}

	@Test
	public void forceCloseTest(){
		client.forceClose();
		//download 
		try {
			client.get().LFile(absoluteLocalPathDownload).RFile(remotePath);
		}catch(Exception e ) {
			assertNotNull(e);
		}
		// delete
		removeRemoteFile();
		client.forceClose();
	}

	@After
	public void removeLocalFile(){
		File f=new File(newFilePath);
		f.delete();
		assertFalse(f.exists());
	}


	
	private void removeRemoteFile() throws RemoteBackendException{
		
		List<StorageObject> list = printDir(remoteDirPath);
		client.remove().RFile(remotePath);
		list = printDir(remoteDirPath);
		assertTrue(list.isEmpty());
	}

	private List<StorageObject>  printDir(String dir) {
		List<StorageObject> list=client.showDir().RDir(dir);
		for(StorageObject obj : list){
			System.out.println("found "+obj.getName()+" and id " +obj.getId());
		}
		return list;
	}

	
	private void setLocalResources() {
		absoluteLocalPath=new File(localPath).getAbsolutePath();
		String dir=new File(absoluteLocalPath).getParent();
		newFilePath=dir+"/testJunit.jpg";
		absoluteLocalPath=new File(localPath).getAbsolutePath();
		absoluteLocalPath2=new File(localPath2).getAbsolutePath();
		absoluteLocalPathDownload=new File(localPathDownload).getAbsolutePath();
	}
	


}
