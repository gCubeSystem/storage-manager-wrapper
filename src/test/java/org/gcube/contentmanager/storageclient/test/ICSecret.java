package org.gcube.contentmanager.storageclient.test;

import org.gcube.common.security.Owner;
import org.gcube.common.security.secrets.Secret;

import java.util.Collections;
import java.util.Map;

public class ICSecret extends Secret{

    private String context;

    protected ICSecret(String context) {
        this.context = context;
    }

    @Override
    public Owner getOwner() {
        return null;
    }

    @Override
    public String getContext() {
        return context;
    }

    @Override
    public Map<String, String> getHTTPAuthorizationHeaders() {
        return Collections.emptyMap();
    }

    @Override
    public boolean isExpired() {
        return false;
    }

    @Override
    public boolean isValid(){ return true; }


}