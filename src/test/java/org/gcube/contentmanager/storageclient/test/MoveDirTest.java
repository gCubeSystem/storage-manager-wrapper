package org.gcube.contentmanager.storageclient.test;

import static org.junit.Assert.*;

import java.io.File;


import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.contentmanagement.blobstorage.service.IClient;
import org.gcube.contentmanagement.blobstorage.transport.backend.RemoteBackendException;
import org.gcube.contentmanager.storageclient.test.utils.Costants;
import org.gcube.contentmanager.storageclient.wrapper.AccessType;
import org.gcube.contentmanager.storageclient.wrapper.StorageClient;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class MoveDirTest {

	private static  String owner="rcirillo";
	private static  String localPath="src/test/resources/CostaRica1.jpg";
	private static  String remoteOriginalFilePath="/test/move/img/original.jpg";
	private static  String remoteOriginalFilePath2="/test/move/img/original2.jpg";
	private static  String remoteMovePath="/test/move/";
	private static  String remoteMovePath2="/test/trash/";
	private static  String remoteMoveFilePath="/test/trash/move/img/original.jpg";
	private static  String remoteMoveFilePath2="/test/trash/move/img/original2.jpg";
	private static  String absoluteLocalPath;
	private static  String newFilePath="src/test/resources";
	private static  IClient client;
//	private String scope="/gcube/devsec";//"/d4science.research-infrastructures.eu";//"/CNR.it/ISTI";//"/gcube"; // "/d4science.research-infrastructures.eu/FARM/VTI";//
	private static  String serviceClass="JUnitTest-MoveDir";
	private static  String serviceName="StorageManager";
	
	@BeforeClass
	public static void getClient() throws RemoteBackendException{
		SecretManagerProvider.set(new ICSecret(Costants.DEFAULT_SCOPE_STRING));
		
		try {
			client=new StorageClient(serviceClass, serviceName, owner, AccessType.SHARED, Costants.DEFAULT_MEMORY_TYPE).getClient();
			assertNotNull(client);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setLocalResources();
		String id=client.put(true).LFile(absoluteLocalPath).RFile(remoteOriginalFilePath);
		assertNotNull(id);
		id=client.put(true).LFile(absoluteLocalPath).RFile(remoteOriginalFilePath2);
		assertNotNull(id);
		id=client.put(true).LFile(absoluteLocalPath).RFile(remoteOriginalFilePath2+"_1");
		assertNotNull(id);
		id=client.put(true).LFile(absoluteLocalPath).RFile(remoteOriginalFilePath2+"_2");
		assertNotNull(id);
		id=client.put(true).LFile(absoluteLocalPath).RFile(remoteOriginalFilePath2+"_3");
		assertNotNull(id);
		id=client.put(true).LFile(absoluteLocalPath).RFile(remoteOriginalFilePath2+"_4");
		assertNotNull(id);
	}
	
	
	@Test
	public void moveTest() throws RemoteBackendException {
		System.out.println("start move");
		long time=System.currentTimeMillis();
//first moved operation	
		client.moveDir().from(remoteMovePath).to(remoteMovePath2);
		long totalTime=System.currentTimeMillis()-time;
		System.out.println("total Time "+totalTime);
		checkOriginalFileIsAlive(remoteOriginalFilePath);
		checkOriginalFileIsAlive(remoteOriginalFilePath2);
		client.get().LFile(newFilePath).RFile(remoteMoveFilePath);
		File f =new File(newFilePath);
		System.out.println("path new File downloaded: "+f.getAbsolutePath());
		assertTrue(f.exists());
		removeLocalFile();
		client.get().LFile(newFilePath).RFile(remoteMoveFilePath2);
		f =new File(newFilePath);
		System.out.println("path new File downloaded: "+f.getAbsolutePath());
		assertTrue(f.exists());
		removeLocalFile();
	}


	private static void setLocalResources() {
		absoluteLocalPath=new File(localPath).getAbsolutePath();
		String dir=new File(absoluteLocalPath).getParent();
		newFilePath=dir+"/testJunitMoveOp.jpg";
		absoluteLocalPath=new File(localPath).getAbsolutePath();
	}

	private static  void removeLocalFile(){
		File f=new File(newFilePath);
		f.delete();
		assertFalse(f.exists());
	}

	private static  void checkOriginalFileIsAlive(String remoteOriginalFilePath) {
		String id=null;
		try{
			id=client.get().LFile(newFilePath).RFile(remoteOriginalFilePath);
		}catch(RemoteBackendException e ){}
		assertNull(id);
	}

	private static void checkMoveFileIsAlive() {
		String id=client.get().LFile(newFilePath).RFile(remoteMovePath);
		System.out.println("id link is alive: "+id);
		assertNotNull(id);
		removeLocalFile();
	}

	@AfterClass
	public static void removeRemoteDirs(){
		client.removeDir().RDir(remoteMovePath2);
	}

}
