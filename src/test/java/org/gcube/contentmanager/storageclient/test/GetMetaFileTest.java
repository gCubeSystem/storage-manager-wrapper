package org.gcube.contentmanager.storageclient.test;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.contentmanagement.blobstorage.resource.RequestObject;
import org.gcube.contentmanagement.blobstorage.resource.StorageObject;
import org.gcube.contentmanagement.blobstorage.service.IClient;
import org.gcube.contentmanagement.blobstorage.transport.backend.RemoteBackendException;
import org.gcube.contentmanager.storageclient.test.utils.Costants;
import org.gcube.contentmanager.storageclient.wrapper.AccessType;
import org.gcube.contentmanager.storageclient.wrapper.StorageClient;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class GetMetaFileTest {

	private static String owner="rcirillo";
	private static String localPath="src/test/resources/CostaRica1.jpg";
	private static String remotePath="/test/img2/CostaRica1.jpg";
	private static String remotePath2="/test/img2/CostaRica2.jpg";
	private static String absoluteLocalPath;
	private static String newFilePath="src/test/resources";
	private static IClient client;
//	private String scope="/gcube/devsec"; //"/d4science.research-infrastructures.eu"; ///gcube/devsec/devVRE"; //"/CNR.it/ISTI";//"/gcube/devsec/devVRE"; // /d4science.research-infrastructures.eu"; //"/d4science.research-infrastructures.eu"; //"/CNR.it/ISTI";//
	private static String serviceClass="JUnitTest";
	private static String serviceName="StorageManager";
	private static String id;
	private static String id2;
	
	@BeforeClass
	public static void getClient() throws RemoteBackendException{
		SecretManagerProvider.set(new ICSecret(Costants.DEFAULT_SCOPE_STRING));
		try {
			client=new StorageClient(serviceClass, serviceName, owner, AccessType.PUBLIC, Costants.DEFAULT_MEMORY_TYPE).getClient();
			assertNotNull(client);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setLocalResources();
		id=client.put(true, "image/jpeg").LFile(absoluteLocalPath).RFile(remotePath);
		id2=client.copyFile().from(remotePath).to(remotePath2);
		assertNotNull(id2);
		assertNotNull(id);
		
	}
	
	
	@Test
	public void getMetaFileByPath() throws RemoteBackendException {
		RequestObject f= client.getMetaFile().RFile(remotePath);
//		System.out.println("mime is: "+f.getMimeType());
		client.put(true, "image/png").LFile("src/test/resources/dog.jpg").RFile(remotePath);
		f= client.getMetaFile().RFile(remotePath);
		 System.out.println("new mime is: "+f.getMimeType());
		assertNotNull(f);
		assertEquals(id, f.getId());
		print(f);
//		f= client.getMetaFile().RFile(remotePath2);
//		assertNotNull(f);
//		assertEquals(id2, f.getId());
//		print(f);
		
	}
	
	@Test
	public void getMetaFileById() throws RemoteBackendException {
		RequestObject f= client.getMetaFile().RFile(id);
		assertNotNull(f);
		assertEquals(id, f.getId());
		print(f);
		f= client.getMetaFile().RFile(id2);
		assertNotNull(f);
		assertEquals(id2, f.getId());
		print(f);

	}
	
	@AfterClass
	public static void removeRemoteFile() throws RemoteBackendException{
		client.remove().RFile(remotePath);
		client.remove().RFile(remotePath2);
		List<StorageObject> list=client.showDir().RDir("test/img2");
		assertTrue(list.isEmpty());
	}
	
	private void print(RequestObject f) {
		System.out.println("\t name "+f.getName());
		System.out.println("\t size "+f.getSize());
		System.out.println("\t owner "+f.getOwner());
		System.out.println("\t id "+f.getId());
		System.out.println("\t absolute remote path "+f.getAbsoluteRemotePath());
		System.out.println("\t remote path: "+f.getRemotePath());
		System.out.println("\t mimetype: "+f.getMimeType());
	}


	private void removeLocalFile(){
		File f=new File(newFilePath);
		f.delete();
		assertFalse(f.exists());
	}


	private static void setLocalResources() {
		absoluteLocalPath=new File(localPath).getAbsolutePath();
		String dir=new File(absoluteLocalPath).getParent();
		newFilePath=dir+"/testJunit.jpg";
		absoluteLocalPath=new File(localPath).getAbsolutePath();
	}

}
