# Changelog for storage-manager-wrapper

## [v4.0.0]
  * removed slf4j implementations, added slf4j-simple;
  * Discovering serviceEndpoint with category Storage
  * moved version from 3.1.0-SNAPSHOT to 4.0.0-SNAPSHOT
  * upgrade to gcube-bom 3 and ScopeProvider removed
  * removed environment variable scope
  * passed to jdk11 and gcube-bom 4.0

## [v3.0.1-SNAPSHOT]
  * removed http protocol;
  * deprecated old smp classes
  * fix JUnitTests
  * upgrade JUnit to 4.12

## [v3.0.0] 2021-09-10
* bug fix #20505
* update gcub-bom version
* Change serviceEndpoint Category from DataStorage to Storage
* bugfix #20505
* adding new constructor with the backendType as input parameter
* retrieving specific backend credentials if a specific backend si specified as input parameter
* moved from 2.6.1 to 3.0.0-SNAPSHOT

## [v2.5.3] 2019-03-20
  * Added wrapper for HomeLibrary configuration related to the new preproduction infrastructure
  
